=======
gvafile
=======

This is the GNUViech Admin fileserver administration tool project.

GNUViech Admin is a suite of tools for server management used for hosting
customer management at `Jan Dittberner IT-Consulting & -Solutions
<http://www.gnuviech-server.de>`_.

Read the :doc:`Installation instructions <install>` to get started locally.

The project page for gvafile is at http://git.dittberner.info/gnuviech/gvafile.
