Changelog
=========

* :release:`0.7.0 <2023-05-07>`
* :support:`-` switch from Pipenv to Poetry

* :release:`0.6.0 <2020-04-10>`
* :support:`-` add Docker setup for lightweight local testing
* :support:`-` update Vagrant setup to libvirt and Debian Buster
* :support:`-` move fileservertasks to top level to keep the task names when
  using Python 3
* :support:`2` use Pipenv for dependency management

* :release:`0.5.1 <2019-09-08>`
* :bug:`-` change dependency URLs

* :release:`0.5.0 <2015-01-29>`
* :feature:`-` add new task set_file_ssh_authorized_keys to add SSH keys for
  users
* :support:`-` improved logging in fileservertasks.tasks, got rid of
  GVAFileException

* :release:`0.4.0 <2015-01-26>`
* :feature:`-` implement new tasks create_file_website_hierarchy and
  delete_file_website_hierarchy
* :support:`-` remove unneeded Django dependencies

* :release:`0.3.0 <2015-01-19>`
* :support:`-` refactor osusers.tasks, move to fileservertasks.tasks

* :release:`0.2.0 <2014-12-27>`
* :support:`-` restrict permissions of mailbox base directories to be read only
* :feature:`-` add new mailbox handling tasks
  :py:func:`osusers.tasks.create_file_mailbox` and
  :py:func:`osusers.tasks.delete_file_mailbox`
* :support:`-` move celery routers into gvacommon that is in it's own
  repository to be used by others (gva, gvaldap)
* :bug:`- major` sftp directories are now owned by root instead of user

* :release:`0.1.0 <2014-12-26>`
* :support:`-` configure celery task serialization, add routing for ldap tasks

* :release:`0.0.2 <2014-12-26>`
* :feature:`-` implement tasks for creating and deleting SFTP and mail base directories

* :release:`0.0.1 <2014-12-25>`
* :feature:`-` initial setup of django application and celery worker
