# -*- coding: utf-8 -*-
class GvaRouter(object):
    def route_for_task(self, task, args=None, kwargs=None):
        for route in ["ldap", "file", "mysql", "pgsql", "web"]:
            if route in task:
                return {"exchange": route, "exchange_type": "direct", "queue": route}
        return None
