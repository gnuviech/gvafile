"""
This module contains :py:mod:`fileservertasks.tasks`.

"""
__version__ = "0.7.0"

from fileservertasks.celery import app as celery_app

__all__ = ("celery_app",)
