==================
Code documentation
==================

gvafile is implemented as a set of `Celery`_ tasks.

.. _Celery: http://www.celeryproject.org/


:py:mod:`fileservertasks` module
================================

.. automodule:: fileservertasks


:py:mod:`celery <fileservertasks.celery>`
-----------------------------------------

.. automodule:: fileservertasks.celery
   :members:


:py:mod:`settings <fileservertasks.settings>`
---------------------------------------------

.. automodule:: fileservertasks.settings
   :members:


:py:mod:`tasks <fileservertasks.tasks>`
---------------------------------------

.. automodule:: fileservertasks.tasks
   :members:
   :undoc-members:
