ARG DEBIAN_RELEASE=buster
FROM debian:$DEBIAN_RELEASE AS builder

ARG GVAAPP=gvafile
ARG POETRY_VERSION=1.3.1

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
      build-essential \
      curl \
      git \
      python3-dev \
      python3-setuptools \
      python3-virtualenv \
      python3-wheel

RUN curl -sSL https://install.python-poetry.org | POETRY_HOME=/root/.local POETRY_VERSION=$POETRY_VERSION python3 - \
    && /root/.local/bin/poetry config virtualenvs.in-project true

WORKDIR /srv/$GVAAPP

COPY poetry.lock pyproject.toml /srv/$GVAAPP/

RUN /root/.local/bin/poetry install --only=main

FROM debian:$DEBIAN_RELEASE
LABEL maintainer="Jan Dittberner <jan@dittberner.info>"

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
       ca-certificates \
       dumb-init \
       gettext \
       python3 \
       python3-pip \
       python3-wheel \
    && apt-get clean \
    && rm -rf /var/cache/apt/archives /var/lib/apt/lists/*

ARG GVAAPP=gvafile
ARG GVAGID=2000
ARG GVAUID=2000

WORKDIR /srv/$GVAAPP

RUN addgroup --gid $GVAGID $GVAAPP ; \
    adduser --home /home/$GVAAPP --shell /bin/bash --uid $GVAUID --gid $GVAGID --disabled-password \
            --gecos "User for gnuviechadmin component $GVAAPP" $GVAAPP

COPY --chown=$GVAAPP:$GVAAPP --from=builder /srv/$GVAAPP/.venv /srv/$GVAAPP/.venv

VOLUME /srv/$GVAAPP/$GVAAPP

COPY ${GVAAPP}.sh entrypoint.sh /srv/

ENTRYPOINT ["dumb-init", "/srv/entrypoint.sh"]
