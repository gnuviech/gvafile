# -*- coding: utf-8 -*-
# pymode:lint_ignore=E501
"""
Common settings and globals.

"""

from os import environ


def get_env_variable(setting):
    """
    Get the environment setting or return exception.

    :param str setting: name of an environment setting
    :raises ImproperlyConfigured: if the environment setting is not defined
    :return: environment setting value
    :rtype: str
    """
    try:
        return environ[setting]
    except KeyError:
        error_msg = "Set the %s env variable" % setting
        raise AssertionError(error_msg)


########## CELERY CONFIGURATION
accept_content = ["json"]
broker_url = get_env_variable("GVAFILE_BROKER_URL")
enable_utc = True
result_backend = get_env_variable("GVAFILE_RESULTS_REDIS_URL")
result_expires = None
result_persistent = True
result_serializer = "json"
task_routes = ("gvacommon.celeryrouters.GvaRouter",)
task_serializer = "json"
timezone = "Europe/Berlin"
########## END CELERY CONFIGURATION

########## GVAFILE CONFIGURATION
GVAFILE_SFTP_DIRECTORY = get_env_variable("GVAFILE_SFTP_DIRECTORY")
GVAFILE_MAIL_DIRECTORY = get_env_variable("GVAFILE_MAIL_DIRECTORY")
GVAFILE_SFTP_AUTHKEYS_DIRECTORY = get_env_variable("GVAFILE_SFTP_AUTHKEYS_DIRECTORY")
########## END GVAFILE CONFIGURATION
