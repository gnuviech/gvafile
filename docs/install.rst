.. index:: installation

=======
Install
=======

Working Environment
===================

To get a running work environment use `pipenv`_.

.. _pipenv: https://pipenv.kennethreitz.org/en/latest/

To get started install `pip` and `pipenv` and use `pipenv install --dev`:

.. code-block:: sh

   $ apt install python3-pip
   $ python3 -m pip install --user -U pipenv
   $ pipenv install --dev

.. index:: celery, worker, file queue

Running the Celery worker
=========================

gvafile uses the `Celery`_ distributed task queue system. The gvafile logic is
executed by a celery worker. After all dependencies are installed you can go
into the gvafile directory and run the celery worker with:

.. code-block:: sh

    $ cd gvafile
    $ pipenv run celery -A filerservertasks worker -Q web -l info

.. _Celery: http://www.celeryproject.org/
