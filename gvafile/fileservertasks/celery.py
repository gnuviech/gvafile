"""
This module defines the Celery_ app for gvafile.

.. _Celery: http://www.celeryproject.org/

"""
from __future__ import absolute_import

from celery import Celery

#: The Celery application
app = Celery('fileservertasks')

app.config_from_object('fileservertasks.settings', namespace="CELERY")
app.autodiscover_tasks(['fileservertasks.tasks'], force=True)
