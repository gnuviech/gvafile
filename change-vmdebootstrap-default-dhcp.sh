#!/bin/sh

set -e

debootstrap_network=/etc/systemd/network/99-dhcp.network

if grep -q '^Name=\\*' "${debootstrap_network}"; then
  primary_nic=$(ls -1 /sys/class/net | grep -v lo |sort | head -1)
  sed -i "s/^Name=e\\*/Name=${primary_nic}/" \
    "${debootstrap_network}"
  systemctl restart systemd-networkd.service
  echo "Changed systemd network configuration"
else
  echo "Systemd network configuration has already been changed"
fi
