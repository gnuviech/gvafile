#!/bin/sh

set -e

QUEUE="file"
TASKS="fileservertasks"
APP="gvafile"
export TZ="Europe/Berlin"

. "/srv/${APP}/.venv/bin/activate"
cd /srv/${APP}/${APP}
celery -A "${TASKS}" worker -Q "${QUEUE}" -E -l info
